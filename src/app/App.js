import React from 'react';
import { Router, Link } from '@reach/router';
import Home from '../pages/Home';
import Counter from '../pages/Counter';
import Todos from '../pages/Todos';
import Test from '../pages/Test';

const App = () => (
  <div>
    <nav>
      <Link to="/">Home</Link>{' '}
      <Link to="counter">Counter</Link>{' '}
      <Link to="todos">Todos</Link>{' '}
      <Link to="test">Test</Link>
    </nav>
    <Router>
      <Home path="/" />
      <Counter path="counter" />
      <Todos path="todos" />
      <Test path="test" />
    </Router>
  </div>
);

export default App;
