import React from 'react';
import External from '../components/External';

const ExternalTest = External('test');

const Test = () => (
  <main>
    <h2>Welcome</h2>
    <ExternalTest />
  </main>
);

export default Test;
