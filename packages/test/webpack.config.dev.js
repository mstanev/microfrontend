const path = require('path');
var devWebPack = require('./webpack.config');

/**
 * Just put dev bundle dirreclly in directory "externals", then main app wiil be hot updated
 */
devWebPack.output.path = path.join(
  `${__dirname}../../../`,
  'externals'
);

module.exports = devWebPack;
